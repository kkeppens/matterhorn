# ApplyACLWorkflowOperationHandler

## Description
The ApplyACLWorkflowOperation will apply the ACL (Access Control List) from series to the mediapackage.

## Operation Example

    <operation
          id="apply-acl"
          fail-on-error="true"
          exception-handler-workflow="error"
          description="Applying access control rules">
    </operation>

